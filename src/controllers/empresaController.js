const Empresas = require('../models/Empresas');
const controller = {};

controller.create = async (req, res) => {
  const payload = req.body;


  try {
    await Empresas.create(payload);
    return res.status(201).json(payload);
  } catch (erro) {
    return res.status(500).json(erro).end();
  }
};

controller.edit = async (req, res) => {
  const {id} = req.company;
  const payload = req.body;
  
  try {
    await Empresas.findByIdAndUpdate(id, payload);
    return res.status(201).end();
  } catch (erro) {
    return res.status(500).json(erro);
  }
};

controller.getOne = async (req, res) => {
  const {company} = req;

  try {

    return res.status(200).json(company);
  } catch (erro) {
    return res.status(500).json(erro);
  }
};

module.exports = controller;
