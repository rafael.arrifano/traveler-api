const mongoose = require('mongoose');

const schema = mongoose.Schema({
  login: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },

  password: {
    type: String,
    required: true,
  },
  token: {
    type: String,
  },

  created_at: {
    type: Date,
    required: true,
  },

  canceled_at: {
    type: Date,
  },

  role: {
    type: String,
    enum: ['admin', 'financeiro', 'expedição'],
    required: true,
  },

  status: {
    type: String,
    enum: ['ativo', 'inativo'],
  },

  company: {
    type: mongoose.ObjectId,
    ref: 'Empresas',
    required: true
  }

});

module.exports = mongoose.model('Users', schema, 'users');
