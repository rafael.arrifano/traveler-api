const Queue = require('bull')
const redis = require('./redis')
const salvarNotas = require('../job/salvarNotas')
const connectDB = require("../configs/database");
const logger = require('./logger')
const dbUser = process.env.DB_USER
const dbName = process.env.DB_NAME
const dbPassword = process.env.DB_PASSWORD
connectDB(`mongodb+srv://${dbUser}:${dbPassword}@cluster0.hk58y.gcp.mongodb.net/${dbName}?retryWrites=true&w=majority`)
const salvarNotasQueue = new Queue(salvarNotas.key, redis)

salvarNotasQueue.on('failed', (job) => {
    logger.warn(job.queue.name, 'Failed', job.data)
})

salvarNotasQueue.on('completed', (job)=>{
    logger.info('Importação finalizada')
})


module.exports = salvarNotasQueue