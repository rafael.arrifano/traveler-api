const mongoose = require('mongoose')
const logger = require('./logger')
mongoose.Promise = Promise;
const connectDB = async (uri) =>{
    try{
        const connect = await mongoose.connect(uri, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        })

        logger.info(`MongoDB Connected: ${connect.connection.host}`)

    }catch(err){
        logger.error(err)
        process.exit(1)
    }
}

module.exports = connectDB
